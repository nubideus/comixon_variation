//
//  ImagesLoadSlider.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class ImagesLoadSlider: ImagesSlider {
    init(urls: [String]) {
        super.init()
        super.images.append(contentsOf: urls.map {
            let image = ImageLoad()
            image.url = $0
            return image
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
