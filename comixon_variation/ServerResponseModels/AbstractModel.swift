//
//  AbstractModel.swift
//  comixon_variation
//
//  Created by evg on 31.08.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

class AbstractModel: Decodable {
    init() {
        fatalError()
    }
}
