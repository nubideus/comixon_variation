//
//  VectorIcon.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import Foundation
import UIKit

class VectorIcon: IBDesignableView {
    private static let DrawBlocksWithountColor: [String: DrawBlockWithoutColor] = [
        "dfadsfa": {_ in}
    ]
    private static let DrawBlocksOneColor: [String: DrawBlockOneColor] = [
        "smallCross": DesignKit.drawSmallCross,
        "bigEmptyAvatarSmile": DesignKit.drawBigEmptyAvatarSmile,
        "commentBubbleTail": DesignKit.drawCommentBubbleTail,
        "filledLike": DesignKit.drawFilledLike,
        "filledComment": DesignKit.drawFilledComment,
        "filledBattle": DesignKit.drawFilledBattle,
        "filledMore": DesignKit.drawFilledMore,
    ]
    private static let DrawBlocksTwoColors: [String: DrawBlock] = [
        "test 2": {_, _, _ in}
    ]
    
    private static let DrawBlocks: [String: DrawBlock] = {
        var blocks = [String: DrawBlock]()
        DrawBlocksTwoColors.forEach{
            blocks[$0.0] = $0.1
        }
        DrawBlocksOneColor.forEach{
            let drawMethod = $0.1
            blocks[$0.0] = { frame, fillColor, _ in drawMethod(frame, fillColor) }
        }
        DrawBlocksWithountColor.forEach{
            let drawMethod = $0.1
            blocks[$0.0] = { frame, _, _ in drawMethod(frame) }
        }
        return blocks
    }()
    
    fileprivate var colors: [UIColor] = [.clear, .clear]
    
    typealias DrawBlockWithoutColor = (_ frame: CGRect) -> ()
    typealias DrawBlockOneColor = (_ frame: CGRect, _ fillColor: UIColor?) -> ()
    typealias DrawBlock = (_ frame: CGRect, _ fillColor: UIColor?, _ fillColor2: UIColor?) -> ()
    
    var drawMethod: DrawBlock! {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var drawMethodIdentifier: String? = nil {
        didSet {
            guard let drawMethodIdentifier = drawMethodIdentifier else { return }
            self.drawMethod = VectorIcon.DrawBlocks[drawMethodIdentifier]
        }
    }
    
    @IBInspectable var stretch: Bool = true {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var iconSize: CGSize = .zero {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var fillColor: UIColor? {
        set {
            colors[0] = newValue ?? UIColor.black
            self.setNeedsDisplay()
        }
        get {
            return colors[0]
        }
    }
    @IBInspectable var fillColor2: UIColor? {
        set {
            colors[1] = newValue ?? UIColor.black
            self.setNeedsDisplay()
        }
        get {
            return colors[1]
        }
    }
    
    @objc convenience init(drawMethod: @escaping DrawBlockWithoutColor){
        self.init(nil, { frame, _, _ in drawMethod(frame) }, [nil, nil])
    }
    
    @objc convenience init(drawMethod: @escaping DrawBlockOneColor, fillColor: UIColor){
        self.init(nil, { frame, color1, _ in drawMethod(frame, color1) }, [fillColor, nil])
    }
    
    @objc convenience init(drawMethod: @escaping DrawBlock, fillColor: UIColor, fillColor2: UIColor){
        self.init(nil, drawMethod, [fillColor, fillColor2])
    }
    
    @objc convenience init(size: CGSize, drawMethod: @escaping DrawBlockWithoutColor){
        self.init(CGRect(x: 0, y: 0, width: size.width, height: size.height), { frame, _, _ in drawMethod(frame) }, [nil, nil])
    }
    
    @objc convenience init(size: CGSize, drawMethod: @escaping DrawBlockOneColor, fillColor: UIColor){
        self.init(CGRect(x: 0, y: 0, width: size.width, height: size.height), { frame, color1, _ in drawMethod(frame, color1) }, [fillColor, nil])
    }
    
    @objc convenience init(size: CGSize, drawMethod: @escaping DrawBlock, fillColor: UIColor, fillColor2: UIColor){
        self.init(CGRect(x: 0, y: 0, width: size.width, height: size.height), drawMethod, [fillColor, fillColor2])
    }
    
    @objc convenience init(){
        self.init(CGRect.zero, nil, [nil, nil])
    }
    
    convenience init(_ width: CGFloat, _ height: CGFloat, _ drawMethod: @escaping DrawBlockOneColor, _ fillColor: UIColor){
        self.init(CGRect(x: 0, y: 0, width: width, height: height), { frame, color1, _ in drawMethod(frame, color1) }, [fillColor, nil])
    }
    
    convenience init(_ width: CGFloat, _ height: CGFloat, _ drawMethod: @escaping DrawBlock, _ fillColor: UIColor, _ fillColor2: UIColor){
        self.init(CGRect(x: 0, y: 0, width: width, height: height), drawMethod, [fillColor, fillColor2])
    }
    
    convenience init(_ drawMethod: @escaping DrawBlockWithoutColor){
        self.init(nil, { frame, _, _ in drawMethod(frame) }, [nil, nil])
    }
    
    convenience init(_ drawMethod: @escaping DrawBlockOneColor, _ fillColor: UIColor){
        self.init(nil, { frame, color1, _ in drawMethod(frame, color1) }, [fillColor, nil])
    }
    
    convenience init(_ drawMethod: @escaping DrawBlock, _ fillColor: UIColor, _ fillColor2: UIColor){
        self.init(nil, drawMethod, [fillColor, fillColor2])
    }
    
    convenience init(_ drawMethodIdentifier: String, _ fillColor: UIColor){
        self.init(nil, nil, [fillColor, .clear])
        self.drawMethod = VectorIcon.DrawBlocks[drawMethodIdentifier]
    }
    
    convenience init(_ drawMethodIdentifier: String, _ fillColor: UIColor, _ fillColor2: UIColor){
        self.init(nil, nil, [fillColor, .clear])
        self.drawMethod = VectorIcon.DrawBlocks[drawMethodIdentifier]
    }
    
    convenience init(_ frame: CGRect? = nil, _ drawMethod: DrawBlock?, _ colors: [UIColor?]){
        self.init(frame: frame ?? CGRect.zero)
        if frame != nil {
            self.stretch = false
        }
        
        self.colors = colors.map{ $0 ?? UIColor.black }
        
        self.iconSize = frame?.size ?? CGSize.zero
    }
    
    override func initView() {
        super.initView()
        self.backgroundColor = .clear
        self.drawMethod = self.drawMethod ?? {_, _, _ in}
        self.isUserInteractionEnabled = true
        self.contentMode = .redraw
    }
    
    override func draw(_ rect: CGRect) {
        let frame = stretch ? rect : CGRect(origin: CGPoint(x: (rect.width - iconSize.width) / 2, y: (rect.height - iconSize.height) / 2), size: iconSize)
        
        drawMethod(frame, colors[0], colors[1])
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return iconSize
        }
    }
}
