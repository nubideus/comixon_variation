//
//  ImageLoaded.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class ImageLoad: UIImageView {
    var url: String? {
        didSet {
            if let urlStr = url, let url = URL(string: urlStr) {
                self.backgroundColor = UIColor.gray
                self.sd_setImage(with: url)
            }
        }
    }
}
