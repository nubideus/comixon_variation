//
//  DesignableView.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

@IBDesignable
class IBDesignableView: UIView {
    func initView() {
        
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    override func prepareForInterfaceBuilder() {
        initView()
    }
}
