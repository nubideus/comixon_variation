//
//  CommentBubbleView.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class CommentBubbleView: IBDesignableView {
    var contentView = UIView()
    private var shapeLayer = CAShapeLayer()
    var leftTailIcon = VectorIcon("commentBubbleTail", .black)
    var rightTailIcon = VectorIcon("commentBubbleTail", .black)
    
    var bubbleColor: UIColor = UIColor.gray {
        didSet {
            leftTailIcon.fillColor = bubbleColor
            rightTailIcon.fillColor = bubbleColor
            shapeLayer.fillColor = bubbleColor.cgColor
        }
    }
    
    override func initView() {
        self.layer.insertSublayer(shapeLayer, at: 0)
        self.insertSubview(leftTailIcon, at: 0)
        self.insertSubview(rightTailIcon, at: 0)
        leftTailIcon.translatesAutoresizingMaskIntoConstraints = false
        rightTailIcon.translatesAutoresizingMaskIntoConstraints = false
        rightTailIcon.transform = .init(scaleX: -1, y: 1)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        contentView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 6).isActive = true
        self.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 6).isActive = true
        self.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        
        leftTailIcon.heightAnchor.constraint(equalToConstant: 21).isActive = true
        leftTailIcon.widthAnchor.constraint(equalToConstant: 12).isActive = true
        rightTailIcon.heightAnchor.constraint(equalToConstant: 21).isActive = true
        rightTailIcon.widthAnchor.constraint(equalToConstant: 12).isActive = true
        leftTailIcon.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        rightTailIcon.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        leftTailIcon.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        rightTailIcon.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        setNeedsDisplay()
    }
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(roundedRect: rect.insetBy(dx: 6, dy: 0), cornerRadius: 17).cgPath
        shapeLayer.path = path
    }
}
