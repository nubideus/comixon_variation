//
//  TagLabel.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class TagLabel: NormalLabel {
    private var insets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    init(tag: String) {
        super.init(frame: .zero)
        self.text = tag
        insets = .init(top: 5, left: 10, bottom: 5, right: 10)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: insets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        return textRect.insetBy(dx: (insets.left + insets.right) * -0.5, dy: (insets.top + insets.bottom) * -0.5)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: insets))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
