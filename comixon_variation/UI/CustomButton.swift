//
//  CustomButton.swift
//  comixon_variation
//
//  Created by evg on 02.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

//
//  CustomButton.swift
//  Comixon
//
//  Created by Vlasenko Evgeniy on 14.09.16.
//  Copyright © 2016 Павел Бахирев. All rights reserved.
//

import Foundation

class CustomButton: UIButton {
    enum CustomState {
        case normal,
        highlighted,
        disabled,
        pressed,
        selected
    }
    
    var title: UILabel? = nil
    
    var selectable: Bool = false
    
    var onChangeStateClosure: ((CustomButton, CustomState) -> ())?
    var pressAction: (() -> ())? = nil
    
    init() {
        super.init(frame: .zero)
        
        self.isUserInteractionEnabled = true
        
        self.addTarget(self, action: #selector(tap), for: .touchUpInside)
    }
    
    @objc func tap() {
        if selectable {
            self.isSelected = !self.isSelected
        } else {
            onChangeStateClosure?(self, .pressed)
            pressAction?()
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                onChangeStateClosure?(self, .highlighted)
            } else {
                onChangeStateClosure?(self, self.isSelected ? .selected : .normal)
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                onChangeStateClosure?(self, .selected)
            } else {
                onChangeStateClosure?(self, .normal)
            }
        }
    }
    
    convenience init(changeState: @escaping (_ selfButton: CustomButton, _ newState: CustomState) -> ()) {
        self.init()
        
        onChangeStateClosure = changeState
        onChangeStateClosure?(self, .normal)
    }
    
    convenience init(
        normaled: ((CustomButton) -> ())? = nil,
        highlighted: ((CustomButton) -> ())? = nil,
        pressed: ((CustomButton) -> ())? = nil,
        selected selectedFn: ((CustomButton) -> ())? = nil,
        disabled: ((CustomButton) -> ())? = nil
        ) {
        self.init(changeState: { _, _ in })
        
        onChangeStateClosure = {
            button, newState in
            
            switch newState {
            case .highlighted:
                highlighted?(button)
            case .selected:
                selectedFn?(button)
            case .disabled:
                disabled?(button)
            case .normal:
                normaled?(button)
            case .pressed:
                pressed?(button)
            }
        }
        onChangeStateClosure?(self, .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
