//
//  ModelRelation.swift
//  comixon_variation
//
//  Created by evg on 25.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

protocol LinkingMachineModelProtocol {
    var restoreIdentifier: String { get }
}

typealias LinkingModel = AbstractModel & LinkingMachineModelProtocol


fileprivate class UpdateListener {
    var handler: ((_ newModel: LinkingModel) -> ())
    
    init(_ handler: @escaping ((_ newModel: LinkingModel) -> ())) {
        self.handler = handler
    }
}

class LinkingMachine<Model: LinkingModel>: LinkingMachineModelProtocol {
    let restoreIdentifier: String
    
    private var sublisteners: [UpdateListener] = []
    
    init(_ model: Model) {
        self.restoreIdentifier = model.restoreIdentifier
        
        if listeners[restoreIdentifier] == nil {
            listeners[restoreIdentifier] = []
        }
        
        if let index = links.firstIndex(where: { ($0.object as? LinkingModel)?.restoreIdentifier == self.restoreIdentifier } ) {
            
        } else {
            links.append(WeakReference(object: model))
        }
        poolgc()
    }
    
    deinit {
        sublisteners = []
        listeners[restoreIdentifier] = listeners[restoreIdentifier]!.filter {
            $0.object !== nil
        }
        
        if listeners[restoreIdentifier]?.count == 0 {
            listeners.removeValue(forKey: restoreIdentifier)
        }
        poolgc()
    }
    
    private func updateModelInPool(_ newModel: Model) {
        if let index = links.firstIndex(where: { ($0.object as? LinkingModel)?.restoreIdentifier == self.restoreIdentifier } ) {
            if links[index].object !== newModel {
                links[index] = WeakReference(object: newModel)
                pushChanges(newModel)
            }
        } else {
            links.append(WeakReference(object: newModel))
        }
        poolgc()
    }
    
    private func poolgc() {
        links = links.filter {
            $0.object !== nil
        }
    }
    
    func restore() -> Self {
        if let model = links.first(where: { ($0.object as? LinkingModel)?.restoreIdentifier == self.restoreIdentifier })?.object as? Model {
            sublisteners.forEach {
                $0.handler(model)
            }
        } else {
            // restore from coredata
        }
        return self
    }
    
    func updated(_ refresher: @escaping (_ model: Model) -> ()) -> Self {
        let newListener = UpdateListener {
            rawModel in
            refresher(rawModel as! Model)
        }
        
        listeners[restoreIdentifier]!.append(WeakReference(object: newListener))
        sublisteners.append(newListener)
        return self
    }
    
    func pushChanges(_ model: Model) {
        if let index = links.firstIndex(where: { ($0.object as? LinkingModel)?.restoreIdentifier == self.restoreIdentifier } ) {
            if links[index].object !== model {
                links[index] = WeakReference(object: model)
            }
        } else {
            links.append(WeakReference(object: model))
        }
        poolgc()
        
        listeners[restoreIdentifier]?.forEach {
            ($0.object as! UpdateListener).handler(model)
        }
    }
}

fileprivate struct WeakReference {
    weak var object: AnyObject?
}

fileprivate var listeners: [String: [WeakReference]] = [:]
fileprivate var links: [WeakReference] = []
