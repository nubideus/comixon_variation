//
//  StripCell.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class StripCell: UITableViewCell {
    private var avatar: Avatar?
    private var slider: ImagesLoadSlider!
    private var tagsView: TagsView?
    private var header: UIView?
    private var stats: StripStatsView?
    
    var strip: Strip? = nil {
        didSet {
            guard let strip = strip else { return }
            self.contentView.subviews.forEach {
                $0.removeFromSuperview()
            }
            avatar = nil
            slider = nil
            tagsView = nil
            header = nil
            
            avatar = Avatar(url: strip.user.userpic.url)
            slider = ImagesLoadSlider(urls: strip.pictures.map { $0.url })
            if strip.tags.count > 0 {
                tagsView = TagsView(tags: strip.tags)
            }
            
            stats = StripStatsView()
            
            header = UIView()
            if let avatar = avatar {
                avatar.translatesAutoresizingMaskIntoConstraints = false
                avatar.widthAnchor.constraint(equalToConstant: 50).isActive = true
                avatar.heightAnchor.constraint(equalToConstant: 50).isActive = true
                header!.addSubview(avatar)
                avatar.leftAnchor.constraint(equalTo: header!.leftAnchor).isActive = true
                avatar.topAnchor.constraint(equalTo: header!.topAnchor).isActive = true
                header!.bottomAnchor.constraint(equalTo: avatar.bottomAnchor, constant: 10).isActive = true
            }
            
            if let slider = slider {
                slider.imageSize = CGSize(width: 200, height: 200)
                slider.heightAnchor.constraint(equalToConstant: 200).isActive = true
            }
            
            let stackView = UIStackView(arrangedSubviews: [header, slider, stats, tagsView].compactMap { $0 })
            stackView.translatesAutoresizingMaskIntoConstraints = false
            
            stackView.axis = .vertical
            stackView.spacing = 10
            self.contentView.addSubview(stackView)
            
            stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
            stackView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
            self.contentView.rightAnchor.constraint(equalTo: stackView.rightAnchor).isActive = true
            let c = self.contentView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
            c.priority = UILayoutPriority.defaultLow
            c.isActive = true
            
            self.contentView.layoutIfNeeded()
        }
    }
}
