//
//  Picture.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class Picture: AbstractModel {
    let url: String
    let preview: String
    let related: String
    
    enum CodingKeys: String, CodingKey {
        case related
        case preview
        case url
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = try container.decode(String.self, forKey: .url)
        self.preview = try container.decode(String.self, forKey: .preview)
        self.related = try container.decode(String.self, forKey: .related)
        try super.init(from: decoder)
    }
}
