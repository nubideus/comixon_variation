//
//  ArrayImplementation.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

class ArrayImplementation<ItemType> {
    enum ArrayImplementationChange {
        case move, delete, insert, reload
    }
    
    private(set) var items: [ItemType]
    typealias ChangeHandler = (_ change: ArrayImplementationChange, _ index: Int, _ item: ItemType?, _ toIndex: Int?, _ meta: AnyObject?) -> ()
    var listeners: [(AnyObject, ChangeHandler)] = []
    
    init(_ items: [ItemType] = []) {
        self.items = items
    }
    
    func deleteItem(at: Int, option: AnyObject? = nil) {
        items.remove(at: at)
        listeners.forEach {
            $0.1(.delete, at, nil, nil, option)
        }
    }
    
    func insertItem(item: ItemType, at: Int = -1, meta: AnyObject? = nil) {
        var at = at
        if at == -1 {
            at = items.count
        }
        items.insert(item, at: at)
        listeners.forEach {
            $0.1(.insert, at, nil, nil, meta)
        }
    }
    
    func reloadItem(at: Int, new item: ItemType? = nil, meta: AnyObject? = nil) {
        if let item = item {
            items[at] = item
        }
        listeners.forEach {
            $0.1(.reload, at, item, nil, meta)
        }
    }
    
    func moveItem(at: Int, to: Int, meta: AnyObject? = nil) {
        let temp = items[to]
        items[to] = items[at]
        items[at] = temp
        listeners.forEach {
            $0.1(.move, at, nil, to, meta)
        }
    }
}
