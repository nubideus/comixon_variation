//
//  SuggestionCell.swift
//  comixon_variation
//
//  Created by evg on 02.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class SuggestionCell: UITableViewCell {
    @IBOutlet weak var avatar: Avatar!
    @IBOutlet weak var cross: VectorIcon!
    @IBOutlet weak var fioLabel: NormalLabel!
    @IBOutlet weak var usernameLabel: NormalLabel!
    @IBOutlet weak var cornerView: IBDesignableView!
}
