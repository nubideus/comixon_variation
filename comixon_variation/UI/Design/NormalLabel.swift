//
//  NormalLabel.swift
//  comixon_variation
//
//  Created by evg on 03.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class NormalLabel: UILabel {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.font = UIFont.systemFont(ofSize: 13)
    }
}
