//
//  PaginationImplementation.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import Foundation

class PaginationImplementation<ItemType: AbstractModel> {
    enum UpdateType {
        case refresh, nextPage, previousPage
    }
    
    public let pageSize = 20
    
    var dataRepresenter: ArrayImplementation<ItemType>!
    var pagesCount = 1
    var loadedPages: CountableClosedRange<Int>? = nil
    var isUpdating = false
    
    var isNeedGetNextPage: Bool {
        get {
            return loadedPages?.upperBound ?? 0 < pagesCount
        }
    }
    
    var startLoad: (() -> ())? = nil
    var willChanges: (() -> ())? = nil
    var didChanges: (() -> ())? = nil
    var didLoad: (() -> ())? = nil
    
    private var request: ApiRequest<Page<ItemType>>? = nil
    
    typealias LoadPageMethod = (_ pageNum: Int) -> (ApiRequest<Page<ItemType>>)
    var loadPageClosure: LoadPageMethod? = nil
    
    var loadLastPage: (_ completion: @escaping (_ success: Bool) -> ()) -> () = {
        completion in
        completion(false)
    }
    
    init(loadPageClosure: LoadPageMethod? = nil, dataRepresenter: ArrayImplementation<ItemType>? = nil) {
        self.loadPageClosure = loadPageClosure
        self.dataRepresenter = dataRepresenter ?? ArrayImplementation()
    }
    
    private func getDataWrapper(updateType: UpdateType, completion: ((_ success: Bool) -> ())? = nil) {
        var nextPageNum: Int
        
        switch updateType {
        case .nextPage:
            nextPageNum = (loadedPages?.upperBound ?? 0) + 1
        case .previousPage:
            nextPageNum = (loadedPages?.lowerBound ?? 2) - 1
        default: nextPageNum = 1
        }
        
        guard let loadPage = loadPageClosure, !isUpdating, 1...pagesCount ~= nextPageNum else {
            completion?(false)
            return
        }
        
        isUpdating = true
        startLoad?()
        self.request = loadPage(nextPageNum).success{
            page in
            
            self.request = nil
            
            var pagesCount = page.pagesCount
            if pagesCount == 0 {
                pagesCount = 1
            }
            self.pagesCount = pagesCount
            
            self.willChanges?()
            switch updateType
            {
            case .refresh:
                self.loadedPages = 1...1
                self.dataRepresenter.items.forEach{
                    _ in
                    self.dataRepresenter.deleteItem(at: 0)
                }
                page.results.forEach{
                    self.dataRepresenter.insertItem(item: $0)
                }
                
            case .nextPage:
                page.results.forEach{
                    self.dataRepresenter.insertItem(item: $0)
                }
                self.loadedPages = (self.loadedPages?.lowerBound ?? 1)...nextPageNum
            case .previousPage:
                page.results.reversed().forEach{
                    self.dataRepresenter.insertItem(item: $0)
                }
                self.loadedPages = nextPageNum...(self.loadedPages?.upperBound ?? 1)
            }
            self.didChanges?()
            
            self.isUpdating = false
            completion?(true)
            self.didLoad?()
        }
    }
    
    func loadNextPage(completion: ((_ success: Bool) -> ())? = nil) {
        getDataWrapper(updateType: .nextPage, completion: completion)
    }
    
    func refreshData(completion: ((_ success: Bool) -> ())? = nil) {
        getDataWrapper(updateType: .refresh, completion: completion)
    }
    
    func loadPrevPage(completion: ((_ success: Bool) -> ())? = nil) {
        getDataWrapper(updateType: .previousPage , completion: completion)
    }
    
    func cancel() {
        request?.cancel()
        request = nil
        self.isUpdating = false
    }
}
