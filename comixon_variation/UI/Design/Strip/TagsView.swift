//
//  TagsView.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class TagsView: UIView {
    var tagsViews: [TagLabel] {
        didSet {
            tagsViews.forEach(self.addSubview)
            setNeedsLayout()
        }
    }
    
    private let tagSpace: CGSize = .init(width: 5, height: 10)
    
    init(tags: [String]) {
        tagsViews = tags.map {
            TagLabel(tag: $0)
        }
        super.init(frame: .zero)
        tagsViews.forEach(self.addSubview)
    }
    
    func layoutTags(width: CGFloat) {
        self.bounds.size.width = width
        self.layoutIfNeeded()
    }
    
    override func layoutSubviews() {
        _ = tagsViews.reduce((CGFloat(0), CGFloat(0))) {
            var (heightAcc, widthAcc) = $0
            $1.sizeToFit()
            if widthAcc + $1.frame.width > self.frame.width && $1 !== self.subviews.first! {
                heightAcc += $1.frame.height + tagSpace.height
                widthAcc = 0
            }
            $1.frame.origin.x = widthAcc
            $1.frame.origin.y = heightAcc
            widthAcc += $1.frame.width + tagSpace.width
            return (heightAcc, widthAcc)
        }
        self.bounds.size = CGSize(width: self.frame.width, height: self.subviews.reduce(CGRect.zero) { $0.union($1.frame) }.height)
        self.invalidateIntrinsicContentSize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return self.bounds.size
        }
    }
}
