//
//  ViewController.swift
//  comixon_variation
//
//  Created by Eugenii Vlasenko on 31.08.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        _ = RestApi.pageFeed(type: .follow, pageNum: 1).success {
//            let strip = $0.results[1]
//            let slider = ImagesLoadSlider(urls: strip.pictures.map{ $0.url })
//            slider.translatesAutoresizingMaskIntoConstraints = false
//            slider.imageSize = CGSize(width: 375, height: 375)
//            
//            self.view.addSubview(slider)
//            slider.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
//            slider.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
//            slider.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
//        }
        
        
//        let view = CommentBubbleView()
//        view.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
//        view.layoutIfNeeded()
//
//        self.view.addSubview(view)
        
        let table = TableViewWithSections()
        table.rowHeight = UITableView.automaticDimension
        let dataLoader = ComixonApi.getFeed(.follow)
        let section = SectionClosures<Strip, StripCell>()
        section.fillCell = {
            cell, item, indexPath in
            cell.strip = item
            
//            cell.contentView.backgroundColor = .red
//            cell.translatesAutoresizingMaskIntoConstraints = false
//            cell.contentView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        }
        
        section.reuseIdentifierForRow = {
            _, _ in
            "stripCell"
        }
        section.registerReuseIdentifiers(["stripCell"])
        section.dataRepresenter = dataLoader.dataRepresenter
        dataLoader.dataRepresenter.listeners.append((self, {
            _ = $4
            section.changesListener($0, $1, $2, $3, UITableView.RowAnimation.top as AnyObject)
        }))
        dataLoader.willChanges = {
            table.beginChanges()
        }
        dataLoader.didChanges = {
            table.endChanges()
        }

        table.insertSections([section])

        self.view.addSubview(table)
        table.frame = self.view.frame

        dataLoader.refreshData()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

