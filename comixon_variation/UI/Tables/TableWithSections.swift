//
//  TableWithSections.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import Foundation
import UIKit

class TableViewWithSections: UITableView, UITableViewDelegate, UITableViewDataSource {
    public private(set) var hasUpdating = false
    
    public private(set) var sections = [SectionProtocol]()
    
    init() {
        super.init(frame: .zero, style: .plain)
        
        self.dataSource = self
        self.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Table
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section].cellForRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        sections[indexPath.section].willDisplay(cell: cell, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].heightForRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[indexPath.section].estimatedHeightForRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        sections[indexPath.section].didSelectRow(cell: cell, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        return sections[indexPath.section].indentationLevelForRow(indexPath: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    // MARK: Edit
    
    //    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //        return sections[indexPath.section].editActionsForRow(indexPath: indexPath).count > 0
    //    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if proposedDestinationIndexPath.section > sourceIndexPath.section {
            return IndexPath(row: self.sections[sourceIndexPath.section].numberOfRows() - 1, section: sourceIndexPath.section)
        } else if proposedDestinationIndexPath.section < sourceIndexPath.section {
            return IndexPath(row: 0, section: sourceIndexPath.section)
        } else {
            return proposedDestinationIndexPath
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        sections[indexPath.section].commitEditingStyle(indexPath: indexPath, editingStyle: editingStyle)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return sections[indexPath.section].editActionsForRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return sections[indexPath.section].editingStyleForRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return sections[indexPath.section].titleForDeleteConfirmationButtonForRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return sections[indexPath.section].shouldIndentWhileEditingRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return sections[indexPath.section].canMoveRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        sections[sourceIndexPath.section].willUserChanges?(.move, sourceIndexPath, destinationIndexPath)
        sections[sourceIndexPath.section].didUserChanges?(.move, sourceIndexPath, destinationIndexPath)
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        sections[indexPath.section].willBeginEditingRow(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        if let indexPath = indexPath {
            sections[indexPath.section].didEndEditingRow(indexPath: indexPath)
        }
    }
    
    // MARK: Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sections[section].headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sections[section].headerView?.bounds.height ?? 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return sections[section].headerView?.bounds.height ?? 0
    }
    
    // MARK: Footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return sections[section].footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return sections[section].footerView?.bounds.height ?? 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return sections[section].footerView?.bounds.height ?? 0
    }
    
    // MARK: sections operations
    func sectionIndex(_ section: SectionProtocol) -> Int? {
        return self.sections.index( where: { $0 === section } )
    }
    
    private func getSectionsIndexes(_ sections: [SectionProtocol]) -> IndexSet {
        var indexSet = IndexSet()
        
        for (index, section) in self.sections.enumerated() {
            if sections.contains(where: { $0 === section}) {
                indexSet.update(with: index)
            }
        }
        
        return indexSet
    }
    
    func reloadSections(_ sections: [SectionProtocol], with animation: UITableView.RowAnimation = .none) {
        self.reloadSections(getSectionsIndexes(sections), with: animation)
    }
    
    func deleteSections(_ sections: [SectionProtocol?], with animation: UITableView.RowAnimation = .none) {
        self.beginUpdates()
        for section in sections.compactMap({ $0 }) {
            if let index = section.index {
                self.sections[index].willRemoveFromTable()
                self.sections.remove(at: index)
                self.deleteSections(IndexSet(integer: index), with: animation)
            }
        }
        self.endUpdates()
    }
    
    func insertSections(_ sections: [SectionProtocol], at: Int = -1, with animation: UITableView.RowAnimation = .none) {
        var at = at
        if at == -1
        {
            at = self.sections.count
        }
        
        self.beginUpdates()
        for section in sections.reversed() {
            if let index = section.index {
                if index != at {
                    self.moveSection(index, toSection: at)
                    continue
                }
            } else {
                self.sections.insert(section, at: at)
                section.willAdd(to: self)
                self.insertSections(IndexSet(integer: at), with: animation)
            }
        }
        self.endUpdates()
    }
    
    // MARK: override batch updating logic
    
    fileprivate var operationsTurn: [TableOperation] = []
    
    private func applyOperations() {
        var deleteSections = [UITableView.RowAnimation: IndexSet]()
        var deleteRows = [UITableView.RowAnimation: [IndexPath]]()
        
        var insertSections = [UITableView.RowAnimation: IndexSet]()
        var insertRows = [UITableView.RowAnimation: [IndexPath]]()
        
        var reloadSections = [UITableView.RowAnimation: IndexSet]()
        var reloadRows = [UITableView.RowAnimation: [IndexPath]]()
        
        var moveSections = [(from: Int, to: Int)]()
        var moveRows = [(from: IndexPath, to: IndexPath)]()
        
        for i in 0..<operationsTurn.count {
            let operation = operationsTurn[i]
            
            // insertSecton(newIndexes)
            // insertRow(newIndexes)
            // deleteSection(oldIndexes)
            // deleteRow(oldIndexes)
            // reloadSection(newIndex)
            // reloadRow(oldIndex)
            // moveSection(???, ???)
            // moveRow(oldIndex, newIndex)
            
            // sections
            if operation.patientType == .section && operation.operationType == .insert {
                if insertSections[operation.animation] == nil {
                    insertSections[operation.animation] = IndexSet()
                }
                insertSections[operation.animation]!.update(with: getSectionAfterUpdate(section: operation.indexPath.section, place: i)!)
            }
            
            if operation.patientType == .section && operation.operationType == .delete {
                if deleteSections[operation.animation] == nil {
                    deleteSections[operation.animation] = IndexSet()
                }
                deleteSections[operation.animation]!.update(with: getSectionBeforeUpdate(section: operation.indexPath.section, place: i))
            }
            
            if operation.patientType == .section && operation.operationType == .reload {
                if reloadSections[operation.animation] == nil {
                    reloadSections[operation.animation] = IndexSet()
                }
                reloadSections[operation.animation]!.update(with: getSectionAfterUpdate(section: operation.indexPath.section, place: i)!)
            }
            
            if operation.patientType == .row && operation.operationType == .move {
                let operation = operation as! MoveTableOperation
                
                moveSections.append((getSectionBeforeUpdate(section: operation.indexPath.section, place: i), getSectionAfterUpdate(section: operation.toIndexPath.section, place: i)!))
            }
            
            // rows
            if operation.patientType == .row && operation.operationType == .insert {
                if insertRows[operation.animation] == nil {
                    insertRows[operation.animation] = [IndexPath]()
                }
                insertRows[operation.animation]!.append(getIndexAfterUpdate(afterIndexPath: operation.indexPath, place: i))
            }
            
            if operation.patientType == .row && operation.operationType == .delete {
                if deleteRows[operation.animation] == nil {
                    deleteRows[operation.animation] = [IndexPath]()
                }
                deleteRows[operation.animation]!.append(getIndexBeforeUpdate(afterIndexPath: operation.indexPath, place: i))
            }
            
            if operation.patientType == .row && operation.operationType == .reload {
                if reloadRows[operation.animation] == nil {
                    reloadRows[operation.animation] = [IndexPath]()
                }
                reloadRows[operation.animation]!.append(getIndexBeforeUpdate(afterIndexPath: operation.indexPath, place: i))
            }
            
            if operation.patientType == .row && operation.operationType == .move {
                let operation = operation as! MoveTableOperation
                
                moveRows.append((getIndexBeforeUpdate(afterIndexPath: operation.indexPath, place: i), getIndexAfterUpdate(afterIndexPath: operation.toIndexPath, place: i)))
            }
        }
        
        deleteRows.forEach {
            super.deleteRows(at: $1, with: $0)
        }
        
        deleteSections.forEach {
            super.deleteSections($1, with: $0)
        }
        
        insertSections.forEach {
            super.insertSections($1, with: $0)
        }
        
        insertRows.forEach {
            super.insertRows(at: $1, with: $0)
        }
        
        reloadSections.forEach {
            super.reloadSections($1, with: $0)
        }
        
        reloadRows.forEach {
            super.reloadRows(at: $1, with: $0)
        }
        
        moveSections.forEach {
            super.moveSection($0.from, toSection: $0.to)
        }
        
        moveRows.forEach {
            super.moveRow(at: $0.from, to: $0.to)
        }
        
        operationsTurn.removeAll()
    }
    
    // MARK: calculate indexes
    
    private func getSectionBeforeUpdate(section: Int, place: Int) -> Int {
        var section = section
        
        for i in (0..<place).reversed() {
            let operation = operationsTurn[i]
            
            if operation.patientType == .section && operation.operationType == .delete && operation.indexPath.section <= section {
                section += 1
            }
            
            if operation.patientType == .section && operation.operationType == .insert && operation.indexPath.section < section {
                section -= 1
            }
        }
        
        return section
    }
    
    private func getSectionAfterUpdate(section: Int, place: Int) -> Int? {
        var section: Int? = section
        var nilMomentIndex: Int? = nil
        
        for i in place+1..<operationsTurn.count {
            let operation = operationsTurn[i]
            
            if operation.patientType == .section && operation.operationType == .delete && section != nil && operation.indexPath.section < section! {
                section = section! - 1
            }
            
            if operation.patientType == .section && operation.operationType == .delete && section != nil && operation.indexPath.section == section! {
                section = nil
                nilMomentIndex = operation.indexPath.section
            }
            
            if operation.patientType == .section && operation.operationType == .insert && section != nil && operation.indexPath.section <= section! {
                section = section! + 1
            }
            
            if operation.patientType == .section && operation.operationType == .insert && nilMomentIndex != nil && operation.indexPath.section == nilMomentIndex! {
                section = nilMomentIndex!
            }
        }
        
        return section
    }
    
    public func getIndexBeforeUpdate(afterIndexPath: IndexPath, place: Int) -> IndexPath {
        var row = afterIndexPath.row
        
        for i in (0..<place).reversed() {
            let operation = operationsTurn[i]
            
            if operation.patientType == .row && operation.operationType == .delete && operation.indexPath.section == afterIndexPath.section && operation.indexPath.row <= row {
                row += 1
            }
            if operation.patientType == .row && operation.operationType == .insert  && operation.indexPath.section == afterIndexPath.section && operation.indexPath.row < row {
                row -= 1
            }
        }
        
        return IndexPath(row: row, section: getSectionBeforeUpdate(section: afterIndexPath.section, place: place))
    }
    
    public func getIndexAfterUpdate(afterIndexPath: IndexPath, place: Int) -> IndexPath {
        var row = afterIndexPath.row
        
        for i in place+1..<operationsTurn.count {
            let operation = operationsTurn[i]
            
            if operation.patientType == .row && operation.operationType == .delete && operation.indexPath.section == afterIndexPath.section && operation.indexPath.row < row {
                row -= 1
            }
            if operation.patientType == .row && operation.operationType == .insert  && operation.indexPath.section == afterIndexPath.section && operation.indexPath.row <= row {
                row += 1
            }
        }
        
        return IndexPath(row: row, section: getSectionAfterUpdate(section: afterIndexPath.section, place: place)!)
    }
    
    private func sectionsOperationWrapped(operationType: TableOperationType, sections: IndexSet, animation: UITableView.RowAnimation) {
        let sections: [Int] = sections.map({ $0 })
        
        for section in sections {
            let indexPath = IndexPath(row: 0, section: section)
            let operation: TableOperation = TableOperation(indexPath: indexPath, operationType: operationType, patientType: .section, animation: animation)
            operationsTurn.append(operation)
        }
        
        if !hasUpdating {
            self.applyOperations()
        }
    }
    
    private func rowsOperationWrapped(operationType: TableOperationType, rows: [IndexPath], animation: UITableView.RowAnimation) {
        for row in rows {
            let operation: TableOperation = TableOperation(indexPath: row, operationType: operationType, patientType: .row, animation: animation)
            operationsTurn.append(operation)
        }
        
        if !hasUpdating {
            self.applyOperations()
        }
    }
    
    override func insertSections(_ sections: IndexSet, with animation: UITableView.RowAnimation) {
        sectionsOperationWrapped(operationType: .insert, sections: sections, animation: animation)
    }
    
    override func deleteSections(_ sections: IndexSet, with animation: UITableView.RowAnimation) {
        sectionsOperationWrapped(operationType: .delete, sections: sections, animation: animation)
    }
    
    override func reloadSections(_ sections: IndexSet, with animation: UITableView.RowAnimation) {
        sectionsOperationWrapped(operationType: .reload, sections: sections, animation: animation)
    }
    
    override func moveSection(_ section: Int, toSection newSection: Int) {
        var newSection = newSection
        
        if section < newSection {
            newSection -= 1
        }
        
        sections.insert(sections.remove(at: section), at: newSection)
        operationsTurn.append(MoveTableOperation(patientType: .section, indexPath: IndexPath(row: 0, section: section), toIndexPath: IndexPath(row: 0, section: newSection)))
    }
    
    override func insertRows(at indexPaths: [IndexPath], with animation: UITableView.RowAnimation) {
        rowsOperationWrapped(operationType: .insert, rows: indexPaths, animation: animation)
    }
    
    override func deleteRows(at indexPaths: [IndexPath], with animation: UITableView.RowAnimation) {
        rowsOperationWrapped(operationType: .delete, rows: indexPaths, animation: animation)
    }
    
    override func reloadRows(at indexPaths: [IndexPath], with animation: UITableView.RowAnimation) {
        rowsOperationWrapped(operationType: .reload, rows: indexPaths, animation: animation)
    }
    
    override func moveRow(at indexPath: IndexPath, to newIndexPath: IndexPath) {
        operationsTurn.append(MoveTableOperation(patientType: .row, indexPath: indexPath, toIndexPath: newIndexPath))
        if !hasUpdating {
            self.applyOperations()
        }
    }
    
    private var updatingStackLength = 0;
    func beginChanges() {
        updatingStackLength += 1
        if !self.hasUpdating {
            hasUpdating = true
            super.beginUpdates()
        }
    }
    
    func endChanges() {
        updatingStackLength = [0, updatingStackLength - 1].max()!
        
        if updatingStackLength == 0 {
            hasUpdating = false
            applyOperations()
            super.endUpdates()
        }
    }
}

fileprivate enum PatientType {
    case section, row
}

fileprivate enum TableOperationType {
    case insert, delete, reload, move
}

fileprivate class TableOperation {
    let indexPath: IndexPath
    let operationType: TableOperationType
    let patientType: PatientType
    let animation: UITableView.RowAnimation
    
    init(indexPath: IndexPath, operationType: TableOperationType, patientType: PatientType, animation: UITableView.RowAnimation) {
        self.indexPath = indexPath
        self.operationType = operationType
        self.patientType = patientType
        self.animation = animation
    }
}

fileprivate class MoveTableOperation: TableOperation {
    let toIndexPath: IndexPath
    
    init(patientType: PatientType, indexPath: IndexPath, toIndexPath: IndexPath) {
        self.toIndexPath = toIndexPath
        
        super.init(indexPath: indexPath, operationType: .move, patientType: patientType, animation: .none)
    }
}
