//
//  Page.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

class Page<Model: AbstractModel>: AbstractModel {
    var results: [Model]
    var pagesCount: Int
    
    enum CodingKeys: String, CodingKey {
        case results
        case pagesCount = "pages"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.results = try container.decode([Model].self, forKey: .results)
        self.pagesCount = try container.decode(Int.self, forKey: .pagesCount)
        try super.init(from: decoder)
    }
}
