//
//  User.swift
//  comixon_variation
//
//  Created by evg on 31.08.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

class BaseUser: AbstractModel {
    let id: Int
    let username: String
    let name: String
    let userpic: Picture
    let iFollowHim: Bool
    let heFollowMe: Bool
    var flag = false
    
    enum BaseUserCodingKeys: CodingKey {
        case id
        case username
        case name
        case userpic
        case iFollowHim
        case heFollowMe
        case location
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: BaseUserCodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.username = try container.decode(String.self, forKey: .username)
        self.name = try container.decode(String.self, forKey: .name)
        self.userpic = try container.decode(Picture.self, forKey: .userpic)
        self.iFollowHim = try container.decode(Bool.self, forKey: .iFollowHim)
        self.heFollowMe = try container.decode(Bool.self, forKey: .heFollowMe)
        try super.init(from: decoder)
    }
}

class FullUser: BaseUser {
    var asd: String
    
    enum FullUserCodingKeys: CodingKey {
        case id
        case username
        case name
        case userpic
        case iFollowHim
        case heFollowMe
        case location
        
        case asd
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: FullUserCodingKeys.self)
        self.asd = try container.decode(String.self, forKey: .asd)
        try super.init(from: decoder)
    }
}

class CurrentUser: FullUser {
    
}
