import Foundation
import Alamofire

class RestApi {
    fileprivate static var server: String = "http://dev.comixon.com/"
    fileprivate static var token: String? = "13dddd7c874b06f27188314513b0f87a4de4bcb9"
    fileprivate static var locale: String { get { return Locale.preferredLanguages[0] } }
    
    @discardableResult static func accountProfile() -> ApiRequest<CurrentUser> {
        return ApiRequest(.get, "account/profile")
    }
    
    @discardableResult static func fullUser(user: BaseUser) -> ApiRequest<FullUser> {
        return ApiRequest(.get, "account/user", parameters: ["id": user.id])
    }
    
    static func pageFeed(type: FeedType, pageNum: Int) -> ApiRequest<Page<Strip>> {
        var params = [String: Any]()
        params["page"] = String(pageNum);
        
        switch type {
            case .follow:
                params["feed"] = "true"
                params["public"] = "True" as AnyObject?
            case .top:
                params["language_zone"] = Locale.preferredLanguages[0];
                params["best"] = "true" as AnyObject?
                params["public"] = "True" as AnyObject?
            case .hellocomixon:
                params["themes__name"] = "hellocomixon"
                params["public"] = "True" as AnyObject?
            case .activity:
                params["activity"] = "true"
            case .favorites:
                params["like"] = "true"
        }
        
        return ApiRequest(.get, "post/post", parameters: params)
    }
}

class ApiRequest<ResponseModel: AbstractModel> {
    private let afRequest: DataRequest
    init(_ httpMethod: Alamofire.HTTPMethod, _ apiMethod: String, parameters: [String: Any]? = nil, asJSON: Bool? = nil) {
        var encoding: ParameterEncoding = Alamofire.URLEncoding.default
        if asJSON ?? false {
            encoding = Alamofire.JSONEncoding.default
        }
        
        var headers = ["Accept-Language": RestApi.locale]
        if RestApi.token != nil {
            headers["Token"] = RestApi.token!
        }
        
        afRequest = Alamofire.request(RestApi.server + apiMethod, method: httpMethod, parameters: parameters, encoding: encoding, headers: headers)
        
        afRequest.validate().responseJSON { (response) in
            self.debugCallbacks.forEach { $0(response.request!, response.response, response.error) }
            switch response.result {
            case .success(let value):
                if let errorMessage = (value as? [String: AnyObject])?["errors_string"] as? String {
                    self.apiErrorCallbacks.forEach { $0(errorMessage) }
                } else {
                    self.successCallbacks.forEach { $0() }
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    do {
                        let model = try decoder.decode(ResponseModel.self, from: response.data!)
                        self.successCallbacks2.forEach { $0(model) }
                    } catch {
                        self.serverErrorCallbacks.forEach { $0() }
                    }
                }
            case .failure(let error):
                if let error = error as? URLError {
                    switch error.code {
                    case .notConnectedToInternet:
                        self.networkErrorCallbacks.forEach { $0() }
                    case .cancelled:
                        // nothing
                        break;
                    default:
                        break;
                    }
                } else {
                    self.serverErrorCallbacks.forEach { $0() }
                }
            }
        }
    }
    
    func cancel() {
        afRequest.cancel()
    }
    
    private var debugCallbacks: [(URLRequest, HTTPURLResponse?, Error?) -> ()] = []
    private var successCallbacks: [() -> ()] = []
    private var successCallbacks2: [(ResponseModel) -> ()] = []
    private var apiErrorCallbacks: [(String) -> ()] = []
    private var serverErrorCallbacks: [() -> ()] = []
    private var networkErrorCallbacks: [() -> ()] = []
    
    func debug(_ callback: @escaping (_ request: URLRequest, _ response: HTTPURLResponse?, _ error: Error?) -> ()) -> Self {
        debugCallbacks.append(callback)
        return self
    }
    func success(_ callback: @escaping () -> ()) -> Self {
        successCallbacks.append(callback)
        return self
    }
    func success(_ callback: @escaping (ResponseModel) -> ()) -> Self {
        successCallbacks2.append(callback)
        return self
    }
    func apiError(_ callback: @escaping (String) -> ()) -> Self {
        apiErrorCallbacks.append(callback)
        return self
    }
    func serverError(_ callback: @escaping () -> ()) -> Self {
        serverErrorCallbacks.append(callback)
        return self
    }
    func networkError(_ callback: @escaping () -> ()) -> Self {
        networkErrorCallbacks.append(callback)
        return self
    }
}
