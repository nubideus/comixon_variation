//
//  Strip.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

class Strip: AbstractModel, LinkingMachineModelProtocol {
    let id: Int
    let user: BaseUser
    let pictures: [Picture]
    let tags: [String]
    
    var restoreIdentifier: String {
        get {
            return "Strip" + String(id)
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case user
        case picture1
        case picture2
        case picture3
        case picture4
        case tags = "themes"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.user = try container.decode(BaseUser.self, forKey: .user)
        var pictures = [Picture]()
        do {
            try [CodingKeys.picture1, .picture2, .picture3, .picture4].forEach {
                pictures.append(try container.decode(Picture.self, forKey: $0))
            }
        } catch {
            // break forEach
        }
        self.pictures = pictures
        self.tags = try container.decode([String].self, forKey: .tags).filter { !$0.isEmpty }
        
        try super.init(from: decoder)
    }
}
