//
//  StripStatsView.swift
//  comixon_variation
//
//  Created by evg on 14.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class StripStatsView: UIView {
    var likeIcon = VectorIcon("filledLike", .black)
    var commentIcon = VectorIcon("filledComment", .black)
    var battleIcon = VectorIcon("filledBattle", .black)
    var moreIcon = VectorIcon("filledMore", .black)
    
    override func layoutSubviews() {
        [likeIcon, commentIcon, battleIcon, moreIcon].forEach {
            $0.widthAnchor.constraint(equalToConstant: 32).isActive = true
            $0.heightAnchor.constraint(equalToConstant: 32).isActive = true
        }
        let stackView = UIStackView(arrangedSubviews: [likeIcon, commentIcon, battleIcon, moreIcon].compactMap { $0 })
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        self.addSubview(stackView)
        
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        super.layoutSubviews()
    }
}
