//
//  Avatar.swift
//  comixon_variation
//
//  Created by evg on 02.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit
import SDWebImage

class Avatar: IBDesignableView {
    var url: String?
    private var ivAvatar: UIView!
    
    let maskShape = CAShapeLayer()
    
    init(url: String?) {
        self.url = url
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initView() {
        self.layer.mask = maskShape
        self.addObserver(self, forKeyPath: "bounds", options: .new, context: nil)
        
        if let urlStr = self.url {
            let image = ImageLoad()
            image.url = urlStr
            ivAvatar = image
        } else {
            ivAvatar = VectorIcon("bigEmptyAvatarSmile", UIColor.red)
        }
        ivAvatar.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(ivAvatar)
        
        ivAvatar.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        ivAvatar.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        ivAvatar.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        ivAvatar.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.layoutIfNeeded()
        self.bounds = self.bounds
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let new = change?[.newKey] as? CGRect, keyPath == "bounds" {
            let cornerRadii = CGSize(
                width: new.width / 2,
                height: new.width / 2
            )
            let maskpath = UIBezierPath(roundedRect: new, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: cornerRadii)
            maskShape.path = maskpath.cgPath
        }
    }
}
