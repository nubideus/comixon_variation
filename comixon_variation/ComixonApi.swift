//
//  ComixonApi.swift
//  comixon_variation
//
//  Created by evg on 02.10.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

enum FeedType {
    case follow, top, hellocomixon, favorites, activity
}

class LinkObject<Model: AbstractModel> {
    
    fileprivate class ModelUpdateListener {
        var handler: ((_ newModel: Model, _ changes: Any?) -> ())
        
        init(_ handler: @escaping ((_ newModel: Model, _ changes: Any?) -> ())) {
            self.handler = handler
        }
    }
    
    fileprivate var listeners: [ModelUpdateListener] = []
    
    func updated(_ handler: @escaping (_ newModel: Model, _ changes: Any?) -> ()) -> Self {
        listeners.append(LinkObject<Model>.ModelUpdateListener(handler))
        return self
    }
}

class ComixonApi {
    static func getFeed(_ feedType: FeedType) -> PaginationImplementation<Strip> {
        let paginator = PaginationImplementation<Strip>(loadPageClosure: {
            RestApi.pageFeed(type: feedType, pageNum: $0)
        })
        
        return paginator
    }
    
    
    private static var userLinks: [LinkObject<BaseUser>] = []
    
    static func userSetupLinking(_ user: BaseUser) -> LinkObject<BaseUser> {
        let link = LinkObject<BaseUser>()
        
        userLinks.append(link)
        return link
    }
    
    static func userDestroyLinking(_ link: LinkObject<BaseUser>) {
        userLinks.removeAll(where: {
            $0 === link
        })
    }
    
    static func userSetFlag(_ user: BaseUser) {
        user.flag = true
        sendUserChanges(user, changes: nil)
    }
    
    private static func sendUserChanges(_ newUser: BaseUser, changes: Any?) {
        userLinks.forEach {
            _ = $0
            $0.listeners.forEach {
                $0.handler(newUser, nil)
            }
        }
    }
}
