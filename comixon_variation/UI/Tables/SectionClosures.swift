//
//  SectionClosures.swift
//  comixon_variation
//
//  Created by evg on 01.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import Foundation
import UIKit

enum UserDataChanges {
    case move, delete, insert
}

//
//@objc protocol SectionCell {
//    @objc optional static var reuseIdentifiers: [String] { get }
//}

protocol SectionProtocol: class {
    var index:Int? { get }
    var headerView: UIView? { get }
    var footerView: UIView? { get }
    
    func willAdd(to table: TableViewWithSections)
    func willRemoveFromTable()
    
    func itemForRow(indexPath: IndexPath) -> Any
    func cellForRow(indexPath: IndexPath) -> UITableViewCell
    func willDisplay(cell: UITableViewCell, indexPath: IndexPath) -> ()
    func didSelectRow(cell: UITableViewCell, indexPath: IndexPath) -> ()
    func heightForRow(indexPath: IndexPath) -> CGFloat
    func estimatedHeightForRow(indexPath: IndexPath) -> CGFloat
    func indentationLevelForRow(indexPath: IndexPath) -> Int
    
    func editingStyleForRow(indexPath: IndexPath) -> UITableViewCell.EditingStyle
    func titleForDeleteConfirmationButtonForRow(indexPath: IndexPath) -> String?
    func editActionsForRow(indexPath: IndexPath) -> [UITableViewRowAction]?
    func shouldIndentWhileEditingRow(indexPath: IndexPath) -> Bool
    func canMoveRow(indexPath: IndexPath) -> Bool
    
    func willBeginEditingRow(indexPath: IndexPath)
    func didEndEditingRow(indexPath: IndexPath?)
    
    func commitEditingStyle(indexPath: IndexPath, editingStyle: UITableViewCell.EditingStyle) -> ()
    
    func numberOfRows() -> Int
    
    var willUserChanges: ((_ editingType: UserDataChanges, _ indexPath: IndexPath, _ secondIndexPath: IndexPath?) -> ())? { get }
    var didUserChanges: ((_ editingType: UserDataChanges, _ indexPath: IndexPath, _ secondIndexPath: IndexPath?) -> ())? { get }
}

class SectionClosures<ItemType: Any, CellType: UITableViewCell>: SectionProtocol {
    private var reuseIdentifiers = [String]()
    
    var headerView: UIView? = nil {
        didSet {
            self.table?.beginUpdates()
            self.table?.endUpdates()
        }
    }
    var footerView: UIView? = nil {
        didSet {
            self.table?.beginUpdates()
            self.table?.endUpdates()
        }
    }
    
    var index: Int? {
        get {
            return self.table?.sectionIndex(self)
        }
    }
    
    weak var table: TableViewWithSections? = nil
    var dataRepresenter: ArrayImplementation<ItemType>! = nil
    
    lazy var changesListener: ArrayImplementation<ItemType>.ChangeHandler = {
        change, index, item, toIndex, option in
        switch change {
        case .insert:
            self.table!.insertRows(at: [IndexPath(row: index, section: self.index!)], with: option as! UITableView.RowAnimation)
        case .move:
            self.table!.moveRow(at: IndexPath(row: index, section: self.index!), to: IndexPath(row: toIndex!, section: self.index!))
        case .delete:
            self.table!.deleteRows(at: [IndexPath(row: index, section: self.index!)], with: option as! UITableView.RowAnimation)
        case .reload:
            self.table!.reloadRows(at: [IndexPath(row: index, section: self.index!)], with: option as! UITableView.RowAnimation)
        }
    }
    
    // MARK: Closures
    
    // typical delegate
    var cellForRow: ((_ indexPath: IndexPath) -> CellType)?
    var fillCell: ((_ cell: CellType, _ item: ItemType, _ indexPath: IndexPath) -> ())?
    var willDisplay: ((_ cell: CellType, _ indexPath: IndexPath) -> ())?
    var didSelectRow: ((_ cell: CellType, _ indexPath: IndexPath) -> ())?
    var heightForRow: ((_ indexPath: IndexPath) -> CGFloat)?
    var estimatedHeightForRow: ((_ indexPath: IndexPath) -> CGFloat)?
    
    // editing
    var editingStyleForRow: ((_ indexPath: IndexPath) -> UITableViewCell.EditingStyle)?
    var titleForDeleteConfirmationButtonForRow: ((_ indexPath: IndexPath) -> String?)? // only with editingStyleForRow
    var editActionsForRow: ((_ indexPath: IndexPath) -> [UITableViewRowAction])?
    var shouldIndentWhileEditingRow: ((_ indexPath: IndexPath) -> Bool)? // not work, use cell.willTransition(to: UITableViewCellStateMask)
    
    var willBeginEditingRow: ((_ indexPath: IndexPath) -> ())?
    var didEndEditingRow: ((_ indexPath: IndexPath?) -> ())?
    
    var canMoveRow: ((_ indexPath: IndexPath) -> Bool)?
    
    var willUserChanges: ((_ editingType: UserDataChanges, _ indexPath: IndexPath, _ secondIndexPath: IndexPath?) -> ())?
    var didUserChanges: ((_ editingType: UserDataChanges, _ indexPath: IndexPath, _ secondIndexPath: IndexPath?) -> ())?
    
    // other
    var indentationLevelForRow: ((_ indexPath: IndexPath) -> Int)?
    var reuseIdentifierForRow: ((_ item: ItemType, _ indexPath: IndexPath) -> String?)?
    var createEmptyItem: ((IndexPath) -> ItemType)?
    
    init(
        cellForRow: ((_ indexPath: IndexPath) -> CellType)? = nil,
        willDisplay: ((_ cell: CellType, _ indexPath: IndexPath) -> ())? = nil,
        didSelectRow: ((_ cell: CellType, _ indexPath: IndexPath) -> ())? = nil,
        heightForRow: ((_ indexPath: IndexPath) -> CGFloat)? = nil,
        reuseIdentifierForRow: ((_ item: ItemType, _ indexPath: IndexPath) -> String?)? = nil
        ) {
        self.cellForRow = cellForRow
        self.willDisplay = willDisplay
        self.heightForRow = heightForRow
        self.didSelectRow = didSelectRow
        self.reuseIdentifierForRow = reuseIdentifierForRow
    }
    
    func willAdd(to table: TableViewWithSections) {
        self.table = table
        register()
    }
    
    func willRemoveFromTable() {
        self.table = nil
    }
    
    func register() {
        for reuseIdentifier in reuseIdentifiers {
            self.table?.register(CellType.self, forCellReuseIdentifier: reuseIdentifier)
        }
    }
    
    func registerReuseIdentifiers(_ ids: [String]) {
        reuseIdentifiers = ids
    }
    
    func dequeueReusableCell(indexPath: IndexPath) -> CellType? {
        var reuseIdentifier: String? = nil
        
        if reuseIdentifierForRow != nil {
            reuseIdentifier = reuseIdentifierForRow!(dataRepresenter.items[indexPath.row], indexPath)
        } else {
            reuseIdentifier = reuseIdentifiers.first
        }
        
        guard let identifier = reuseIdentifier else {
            return nil
        }
        
        return table?.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? CellType
    }
    
    
    func itemForRow(indexPath: IndexPath) -> Any {
        return dataRepresenter.items[indexPath.row] as Any
    }
    
    func getCalculatedCellHeight(indexPath: IndexPath) -> CGFloat {
        let cell = self.cellForRow(indexPath: indexPath);
        let height = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        return height
    }
    
    // MARK: Typical table delegate
    func cellForRow(indexPath: IndexPath) -> UITableViewCell {
        let cell = self.cellForRow?(indexPath) ?? self.dequeueReusableCell(indexPath: indexPath)!
        
        fillCell?(cell, dataRepresenter.items[indexPath.row], indexPath)
        
        return cell
    }
    
    func willDisplay(cell: UITableViewCell, indexPath: IndexPath) {
        guard let cell = cell as? CellType else { return }
        
        self.willDisplay?(cell, indexPath)
    }
    
    func didSelectRow(cell: UITableViewCell, indexPath: IndexPath) {
        guard let cell = cell as? CellType else { return }
        
        self.didSelectRow?(cell, indexPath)
    }
    
    func heightForRow(indexPath: IndexPath) -> CGFloat {
        return self.heightForRow?(indexPath) ?? UITableView.automaticDimension
    }
    
    internal func estimatedHeightForRow(indexPath: IndexPath) -> CGFloat {
        return self.estimatedHeightForRow?(indexPath) ?? self.heightForRow?(indexPath) ?? UITableView.automaticDimension
            //getCalculatedCellHeight(indexPath: indexPath)
    }
    
    func indentationLevelForRow(indexPath: IndexPath) -> Int {
        return self.indentationLevelForRow?(indexPath) ?? 0
    }
    
    internal func numberOfRows() -> Int {
        return dataRepresenter.items.count
    }
    
    // MARK: Edit configuration
    
    func editingStyleForRow(indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return self.editingStyleForRow?(indexPath) ?? .none
    }
    
    func titleForDeleteConfirmationButtonForRow(indexPath: IndexPath) -> String? {
        return self.titleForDeleteConfirmationButtonForRow?(indexPath) ?? nil
    }
    
    func editActionsForRow(indexPath: IndexPath) -> [UITableViewRowAction]? {
        return self.editActionsForRow?(indexPath) ?? nil
    }
    
    func shouldIndentWhileEditingRow(indexPath: IndexPath) -> Bool {
        return self.shouldIndentWhileEditingRow?(indexPath) ?? true
    }
    
    func canMoveRow(indexPath: IndexPath) -> Bool {
        return self.canMoveRow?(indexPath) ?? false
    }
    
    // MARK: Edit operations
    
    func commitEditingStyle(indexPath: IndexPath, editingStyle: UITableViewCell.EditingStyle) -> () {
        if editingStyle == .insert {
            self.willUserChanges?(.insert, indexPath, nil)
            self.dataRepresenter.insertItem(item: self.createEmptyItem!(indexPath), at: indexPath.row)
            self.didUserChanges?(.insert, indexPath, nil)
        } else if editingStyle == .delete {
            self.willUserChanges?(.delete, indexPath, nil)
            self.dataRepresenter.deleteItem(at: indexPath.row)
            self.didUserChanges?(.delete, indexPath, nil)
        }
    }
    
    func willBeginEditingRow(indexPath: IndexPath) {
        self.willBeginEditingRow?(indexPath)
    }
    
    func didEndEditingRow(indexPath: IndexPath?) {
        self.didEndEditingRow?(indexPath)
    }
}
