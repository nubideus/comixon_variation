//
//  Slider.swift
//  comixon_variation
//
//  Created by evg on 12.09.2018.
//  Copyright © 2018 Eugenii Vlasenko. All rights reserved.
//

import UIKit

class ImagesSlider: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var images = [UIImageView]()
    var imageSize: CGSize! {
        didSet {
            let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
            
            layout.itemSize = imageSize
        }
    }
    
    var infinity: Bool = false
    
    init() {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        self.backgroundColor = .clear
        
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        self.showsHorizontalScrollIndicator = false
        self.delegate = self
        self.dataSource = self
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "imageCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let layout = (collectionViewLayout as! UICollectionViewFlowLayout)
        if infinity && (section <= 1 || section >= self.numberOfSections(in: self) - 2) {
            var inset = (collectionViewLayout as! UICollectionViewFlowLayout).sectionInset
            if section == 0 {
                inset.right = 0
            }
            if section == 1 {
                inset.left = layout.minimumLineSpacing
            }
            if section == self.numberOfSections(in: self) - 2 {
                inset.right = layout.minimumLineSpacing
            }
            if section == self.numberOfSections(in: self) - 1 {
                inset.left = 0
            }
            return inset
        }
        return (collectionViewLayout as! UICollectionViewFlowLayout).sectionInset
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if infinity {
            return 3
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if infinity && (section == 0 || section == self.numberOfSections(in: self) - 1) {
            return 2
        } else {
            return images.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let numberOfSections = collectionView.numberOfSections - 2
        
        if infinity && (indexPath.section == 0 || indexPath.section == numberOfSections + 1) {
            let numberOfSections = collectionView.numberOfSections - 2
            
            let fakeSection: Int
            let fakeRow: Int
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    let section = numberOfSections
                    let row = 0
                    let cellAttr = self.layoutAttributesForItem(at: IndexPath(row: row, section: section))!
                    self.contentOffset.x += (cellAttr.frame.maxX - cell.frame.maxX)
                    return
                }
                fakeSection = numberOfSections
                fakeRow = collectionView.numberOfItems(inSection: fakeSection) - (2 - indexPath.row)
            } else {
                if indexPath.row == 1 {
                    let section = 1
                    let row = 1
                    let cellAttr = self.layoutAttributesForItem(at: IndexPath(row: row, section: section))!
                    self.contentOffset.x += (cellAttr.frame.minX - cell.frame.minX)
                    return
                }
                fakeSection = 1
                fakeRow = indexPath.row
            }
            
            let fakeIndexPath = IndexPath(row: fakeRow, section: fakeSection)
            return self.collectionView(collectionView, willDisplay: cell, forItemAt: fakeIndexPath)
        }
        
        if cell.contentView.subviews.count == 0 {
            let imageView = images[indexPath.row]
            cell.addSubview(imageView)
            imageView.frame = cell.contentView.frame
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.contentView.subviews.forEach {
            $0.removeFromSuperview()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            return imageSize
        }
    }
}
